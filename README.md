<div align="center">

# 👤**About Me**
🖥️ **I'm currently studying to be a fullstack JavaScript developer**

🖥️ **I’m currently learning : Node.js, Postgresql and Docker**

📓 **I'm graduating in computer science at the university of Vilha Velha**

</div>
<div align="center">

# 💻Tech Stack
![JavaScript](https://img.shields.io/badge/javascript-darkslategray.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E) ![TypeScript](https://img.shields.io/badge/typescript-darkturquoise.svg?style=for-the-badge&logo=typescript&logoColor=#0074c9) ![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white) ![NestJS](https://img.shields.io/badge/nest.js-crimson.svg?style=for-the-badge&logo=nestjs&logoColor=white) ![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white) ![Postgresql](https://img.shields.io/badge/postgresql-darkslateblue.svg?style=for-the-badge&logo=postgresql&logoColor=white) ![Postman](https://img.shields.io/badge/postman-chocolate.svg?style=for-the-badge&logo=postman&logoColor=white) ![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white) ![Tomato](https://img.shields.io/badge/git-tomato.svg?style=for-the-badge&logo=git&logoColor=white)

</div>
